# `/home/sam/.local/dotfiles`

This repo contains scripts and configuration for various programs I use.

Maybe you'll also be interested in my [website](https://afuera.me.uk).

## Installation

Make sure you have cloned with all submodules available.

Then use Meson to install:

    mkdir build
    cd build
    meson .. --prefix=$HOME
    ninja install

## License

All code within is [MIT licensed](https://en.wikipedia.org/wiki/MIT_License)
unless otherwise noted.
