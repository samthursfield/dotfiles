alias edit = gvim --remote-tab

alias ga = git add
alias gb = git branch
alias gcp = git cherry-pick
alias gd. = git diff
alias gd.c = git diff --cached
alias gco = git checkout
alias gci = git commit
alias gd. = git diff
alias gd.c = git diff --cached
alias gl = git log
alias gm.n = git merge --no-ff --no-commit
alias gp = git
alias gpo = git push origin
alias gpf = git push fork
alias gpw = git push write
alias gr. = git rebase
alias gr.a = git rebase --abort
alias gr.c = git rebase --continue
alias gr.i = git rebase --autosquash --interactive
alias grup = git remote update --prune
alias gs = git status

$env.config.cursor_shape = {
    emacs: line # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (line is the default)
    vi_insert: block # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (block is the default)
    vi_normal: underscore # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (underscore is the default)
}

$env.config.edit_mode = "vi"
